<?php

trait hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi(){
        echo "{$this->nama} sedang {$this->keahlian}";
    }
}

abstract class fight {
    use hewan;
    public $attackPower;
    public $defencePower;

    public function serang($hwn){
        echo "{$this->nama} sedang menyerang {$hwn->nama}";

        $hwn->diserang($this);
    }

    public function diserang($hwn){
        echo "{$this->nama} sedang diserang {$hwn->nama}";


        $this->darah = $this->darah - ($hwn->attackPower / $this->defencePower);
    }

    abstract public function getInfoHewan();

}

class elang extends fight{

    public function __construct($nama){
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defensePower = 5;
    }

    public function getInfoHewan(){
        echo "Jenis Hewan : Elang";
        echo "<br>";
        echo "Nama :{$this->nama} ";
        echo "<br>";
        echo "Jumlah Kaki : {$this->jumlahKaki}";
        echo "<br>";
        echo "Keahlian : {$this->keahlian}";
        echo "<br>";
    }

}

class harimau extends fight{

    public function __construct($nama){
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat ";
        $this->attackPower = 7;
        $this->defensePower = 8;
    }

    public function getInfoHewan(){
        echo "Jenis Hewan : Harimau";
        echo "<br>";
        echo "Nama :{$this->nama} ";
        echo "<br>";
        echo "Jumlah Kaki : {$this->jumlahKaki}";
        echo "<br>";
        echo "Keahlian : {$this->keahlian}";
    }

}

$harimau = new Harimau("harimau sumatera");
$harimau->getInfoHewan();

echo "<br>";
echo "<br>";
echo "<br>";

$elang = new Elang('elang');
$elang->getInfoHewan();

echo "<br>";
echo "<br>";
echo "<br>";


$harimau->serang($elang);

?>