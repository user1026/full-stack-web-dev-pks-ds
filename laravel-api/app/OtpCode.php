<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use illuminate\support\Str;

class OtpCode extends Model
{
    protected $fillable = ['id', 'otp', 'user_id'];
    
    public function users()
    {
        return $this->belongsTo('App\users');
    }  
    
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if (empty($model->{$model->getKeyname()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
}
