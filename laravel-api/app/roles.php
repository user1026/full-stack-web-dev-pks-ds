<?php

namespace App;

use illuminate\support\Str;
use Illuminate\Database\Eloquent\Model;

class roles extends Model
{
    protected $fillable = ['name', 'id'];
    
    public function users()
    {
        return $this->hasMany('App\users');
    }  

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if (empty($model->{$model->getKeyname()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
}
