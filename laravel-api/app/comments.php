<?php

namespace App;

use illuminate\support\Str;
use Illuminate\Database\Eloquent\Model;

class comments extends Model
{
    protected $fillable = ['id', 'content','post_id'];

    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if (empty($model->{$model->getKeyname()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
}
