<?php

namespace App;

use illuminate\support\Str;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'description', 'id'];
    
    public function comments()
    {
        return $this->hasMany('App\comments');
    }  
    
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if (empty($model->{$model->getKeyname()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
}
