<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\users;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        dd('masuk');

        $allRequest = $request->all();

        $validator = Validator::make($allRequest,[
            'otp_code' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $otp_code = OtpCode::where('otp' , $request->otp)->first();

        $now = Carbon::now();

        if(!$otp_code){
            return response()->json([
                'success' => false,
                'message' => 'OTP code tidak ditemukan' 
            ],400);
        }

        if ($now > $otp_code->valid_until){
            return response()->json([
                'success' => false,
                'message' => 'OTP code kadaluwarsa' 
            ],400);
        }

        $user = User::find($otp_code->user_id);
        $user->update([
            'email_verified_at' => $now
        ]);

        $otp_code->delete();
        
        return response()->json([
            'succes' => true,
            'message' => 'User telah terverifikasi',
            'data' => $user
        ]);
    
    }
}
