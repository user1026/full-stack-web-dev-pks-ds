<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\users;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest,[
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = users::where('email', $request->email)->first();
        
        
        $user->otp_code->delete();
        dd($user->otp_code->delete());

        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp',$random)->first();
        } while ($check);
    }
}
