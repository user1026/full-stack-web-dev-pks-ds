<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\users;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class UpdatePassword extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    { dd('masuk');
        $allRequest = $request->all();

        $validator = Validator::make($allRequest,[
            'email' => 'required',
            'password' => 'required|confirmed|min:6'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        users::where('email', $request->email)->first();
        if(!$user){
            return response()->json([

                'success' => false,
                'message' => 'User tidak ditemukan'
            ], 400);
        }

        $user->update([
            'password' => Hash::make($request->password)
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Password berhasil diubah'
        ] , 200);
    }
}
