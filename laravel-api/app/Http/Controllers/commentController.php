<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\comments;
use Illuminate\Support\Facades\Validator;

class commentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get data from table comments
        $comments = comments::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data comment',
            'data'    => $comments  
        ], 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comments = comments::create([
            'content'     => $request->content,
            'post_id'   => $request->post_id
        ]);

        //success save to database
        if($comments) {

            return response()->json([
                'success' => true,
                'message' => 'comments Created',
                'data'    => $comments  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'comments Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         //find comments by ID
         $comments = comments::findOrfail($id);

         //make response JSON
         return response()->json([
             'success' => true,
             'message' => 'Detail Data comments',
             'data'    => $comments 
         ], 200); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find comments by ID
        $comments = comments::findOrFail($id);

        if($comments) {

            //update comments
            $comments->update([
                'content'     => $request->content,
                'post_id'   => $request->post_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'comments Updated',
                'data'    => $comments  
            ], 200);

        }

        //data comments not found
        return response()->json([
            'success' => false,
            'message' => 'comments Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         //find comment$comments by ID
         $comments = comments::findOrfail($id);

         if($comments) {
 
             //delete comment$comments
             $comments->delete();
 
             return response()->json([
                 'success' => true,
                 'message' => 'comments Deleted',
             ], 200);
 
         }
 
         //data comment$comments not found
         return response()->json([ 
             'success' => false,
             'message' => 'comments Not Found',
         ], 404);
     }
}

