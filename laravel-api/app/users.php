<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use illuminate\support\Str;

class users extends Model
{
    protected $fillable = ['email','username', 'id','role_id','name','password'];
    public function roles()
    {
        return $this->belongsTo('App\roles');
    }
    
    public function otp_codes()
    {
        return $this->hasOne('App\OtpCode');
    }

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if (empty($model->{$model->getKeyname()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
}
