//Soal 1
console.log("-=== SOAL 1 ===-");
const hitung = (p,l) => 
{
    const luas = p * l;
    const kel = 2 * (p+l);
    let text = `Luas = ${luas}, dan Keliling = ${kel}`;
    return text;
}

console.log(hitung(4,4));

//Soal 2 
console.log("-=== SOAL 2 ===-");
const nama = (firstName, lastName) => {
    return console.log(`${firstName} ${lastName}`);
    }
   
  //Driver Code 
  nama("William", "Imoh"); 

//Soal 3
console.log("-=== SOAL 3 ===-");
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  };

const {firstName,lastName,address,hobby} = newObject;

    // Driver code
    console.log(firstName, lastName, address, hobby);

//Soal 4
console.log("-=== SOAL 4 ===-");

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
let combined = [...west,...east];


//Driver Code
console.log(combined);

//Soal 5
console.log("-=== SOAL 5 ===-");

const planet = "earth"; 
const view = "glass" ;
var before = `Lorem  ${view}  dolor sit amet, consectetur adipiscing elit,  ${planet}`;

//Driver code 
console.log(before);