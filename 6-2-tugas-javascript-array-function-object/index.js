//  Soal 1
console.log("---> SOAL 1 <---")
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
var panjangArray = daftarHewan.length;

for(var i = 0; i < panjangArray; i++){
    console.log(daftarHewan[i]);
}

//Soal 2
console.log("---> SOAL 2 <---");
function introduce(data) {
    return "Nama saya "+ data.name + " umur saya "+data.age+" tahun, alamat saya di "+ data.adress +", dan saya punya hobby yaitu "+data.hobby+ " !";
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" };

var perkenalan = introduce(data);
console.log(perkenalan);

//Soal 2
console.log("---> SOAL 3 <---");

var vokal = "aiueoAIUEO";
function hitung_huruf_vokal(str) {
    var cnt = 0;
    
    for(i=0; i<str.length; i++){
        
        if(vokal.includes(str[i])){
            
            cnt++;
   
        }
   
    }
   
    return cnt;
}

var hitung1 = hitung_huruf_vokal("Muhammad");
var hitung2 = hitung_huruf_vokal("Iqbal");
console.log(hitung1,hitung2);

//Soal 4
console.log("---> SOAL 4 <---");
function hitung(int) {
    
    if (int===0){
        return -2;
    }

    else if(int>0){
        return int + 2;
    }
}
console.log(hitung(0));